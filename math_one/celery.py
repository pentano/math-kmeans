from __future__ import absolute_import, unicode_literals

from celery import Celery

from django.conf import settings

import os


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'math_one.settings')

app = Celery(
    'math_one',
    backend='rpc://',
    broker=settings.CELERY_BROKER_URL,
)

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()
from cluster.models import *
from django.contrib.auth import get_user_model
from model_mommy import mommy
from numpy.testing import assert_array_equal
from numpy.testing import assert_array_almost_equal
import pandas as pd
import pytest
from random import randrange
from sklearn.decomposition import PCA
from unittest import TestCase


class ClusterManagerTest(TestCase):

    @pytest.fixture()
    def setup(self):
        conversation = mommy.make(Conversation)

        comment = mommy.make(
            'Comment',
            conversation=conversation,
        )

        cm = ClusterManager(conversation)
        return (cm, comment)

    def create_votes(self, comment, n_votes, last_user_id=0):
        votes = []
        for i in range(1, n_votes + 1):
            value = randrange(-1, 2, 1)
            vote = mommy.make(
                'Vote',
                comment=comment,
                user_id=last_user_id + i,
                value=value
            )
            votes.append(vote)

        return votes

    @pytest.mark.django_db
    def test_ready_to_math_should_return_false_for_n_participants_belw_4(self):
        cm, comment = self.setup()
        # 1 vote
        self.create_votes(comment, 1)
        assert not cm.ready_to_math()

        # 2 votes
        self.create_votes(comment, 1, 1)
        assert not cm.ready_to_math()

        # 3 votes
        self.create_votes(comment, 1, 2)
        assert not cm.ready_to_math()

    @pytest.mark.django_db
    def test_ready_to_math_should_return_true_for_n_participants_above_3(self):
        cm, comment = self.setup()
        # 4 votes
        self.create_votes(comment, 4)
        assert cm.ready_to_math()

        # 5 votes
        self.create_votes(comment, 1, 4)
        assert cm.ready_to_math()

    @pytest.mark.django_db
    def test_should_get_dataset_for_1_vote_1_participant_1_comment(self):
        cm, comment = self.setup()
        votes = self.create_votes(comment, 1)
        dataframe = pd.DataFrame(
            data=[votes[0].value],
            index=[1],
            columns=[comment.pk],
        )

        reduced_data = cm.get_reduced_dataset()
        assert_array_equal(dataframe.get_values(), reduced_data.get_values())
        assert_array_equal(dataframe.index, reduced_data.index)
        assert_array_equal(dataframe.columns, reduced_data.columns)

    @pytest.mark.django_db
    def test_should_get_dataset_6_participants_in_2_comments(self):
        cm, comment = self.setup()
        votes_c1 = self.create_votes(comment, 6)

        comment2 = mommy.make(
            'Comment',
            conversation=cm.conversation
        )
        votes_c2 = self.create_votes(comment2, 6)

        data = cm.get_reduced_dataset()
        assert_array_equal([1, 2, 3, 4, 5, 6], data.index)
        assert comment.pk in data.columns
        assert comment2.pk in data.columns

        # The created dataframe does not follow a pattern in columns order
        votes_data = []
        if(data.columns[0] == comment.pk):
            votes_data = [
                [votes_c1[i].value, votes_c2[i].value]
                for i in range(0, 6)
            ]
        else:
            votes_data = [
                [votes_c2[i].value, votes_c1[i].value]
                for i in range(0, 6)
            ]

        assert_array_equal(votes_data, data.get_values())

    @pytest.mark.django_db
    def test_should_get_complete_dataset_6_participants_in_3_comments(self):
        cm, comment = self.setup()
        votes_c1 = self.create_votes(comment, 6)

        comment2 = mommy.make(
            'Comment',
            conversation=cm.conversation
        )
        votes_c2 = self.create_votes(comment2, 6)

        comment3 = mommy.make(
            'Comment',
            conversation=cm.conversation
        )
        votes_c3 = self.create_votes(comment3, 6)

        data = cm.get_reduced_dataset()
        assert_array_equal([1, 2, 3, 4, 5, 6], data.index)
        assert_array_equal(['X', 'Y'], data.columns)

        votes_data = [
            [votes_c1[i].value, votes_c2[i].value, votes_c3[i].value]
            for i in range(0, 6)
        ]
        dataframe = pd.DataFrame(
            data=votes_data,
            index=[1, 2, 3, 4, 5, 6],
            columns=[comment.pk, comment2.pk, comment3.pk],
        )

        decomposition = PCA(n_components=2)
        reduced_dataset = decomposition.fit_transform(dataframe)
        assert_array_almost_equal(reduced_dataset, data.get_values(), 8)

from cluster.models import *
from model_mommy import mommy

import numpy as np
import pytest

import csv
import re
from os.path import dirname, join
from sklearn import metrics


@pytest.fixture()
def setup(request):
    np.random.seed(57)
    conversation = mommy.make(Conversation)

    comment = mommy.make(
        'Comment',
        conversation=conversation,
    )

    vote = mommy.prepare(
        'Vote',
        comment=comment,
        user_id=1,
        value=1,
    )

    cm = ClusterManager(vote)
    return (cm)

# Accuracy Tests #
"""
    This tests run against Polis Data and are just to generate a report, not
    to assert anything.
"""


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_50_comments_2_clusters_5_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 50,
        'n_members': 5,
        'n_populations': 2,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_50_comments_2_clusters_10_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 50,
        'n_members': 10,
        'n_populations': 2,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_150_comments_2_clusters_10_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 150,
        'n_members': 10,
        'n_populations': 2,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_50_comments_3_clusters_5_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 50,
        'n_members': 5,
        'n_populations': 3,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_50_comments_3_clusters_10_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 50,
        'n_members': 10,
        'n_populations': 3,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_150_comments_3_clusters_10_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 150,
        'n_members': 10,
        'n_populations': 3,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_50_comments_4_clusters_5_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 50,
        'n_members': 5,
        'n_populations': 4,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_50_comments_4_clusters_10_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 50,
        'n_members': 10,
        'n_populations': 4,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_150_comments_4_clusters_10_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 150,
        'n_members': 10,
        'n_populations': 4,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_50_comments_5_clusters_5_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 50,
        'n_members': 5,
        'n_populations': 5,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_50_comments_5_clusters_10_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 50,
        'n_members': 10,
        'n_populations': 5,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


@pytest.mark.django_db
@pytest.mark.accuracy
@pytest.mark.slow
def test_compare_accuracy_150_comments_5_clusters_10_members(setup, mocker):
    cm = setup
    cm.k_max = 5
    state = {
        'n_comments': 150,
        'n_members': 10,
        'n_populations': 5,
        'pca_components': 2,
    }

    state['accuracy'] = calculate_accuracy(cm, state, mocker)
    display_results(state)


def display_results(state):
    print(
        "\nAccuracy: %s, Populations: %s, Comments: %s, Members: %s, Total Participants: %s" % (
             state['accuracy'],
             state['n_populations'],
             state['n_comments'],
             state['n_members'],
             state['n_members'] * state['n_populations'],
        )
    )


def calculate_accuracy(cm, state, mocker):
    populations_probabilities = generate_populations_probabilities(
        state['n_comments'],
        state['n_populations']
    )

    votes_data = []
    expected_labels = []

    # Generate users votes for each comment and fill expected clusters
    for i, pop in enumerate(populations_probabilities):
        votes = generate_members_votes_for_pop(
            pop,
            state['n_members'],
            state['n_comments'],
        ) 
        votes_data.extend(votes)
        # Each pop belongs to a cluster label, assigned from 0 to n_populations
        labels_for_pop = np.full(state['n_members'], i)
        expected_labels.extend(labels_for_pop)

    decomposition = PCA(n_components=state['pca_components'])
    reduced_data = decomposition.fit_transform(votes_data)

    mocker.patch.object(
        cm,
        'get_reduced_dataset',
        return_value=reduced_data
    )

    cluster_data = cm.compute_clusters()
    rand_index = metrics.adjusted_rand_score(expected_labels, cluster_data.labels)

    return rand_index.round(2)


def generate_members_votes_for_pop(pop=None, n_members=10, n_comments=10):
    votes_data = []
    for user in range(n_members):
        choices = np.ones(n_comments)
        user_prob = np.random.uniform(size=n_comments)
        choices[user_prob < pop[:, 1]] = 0
        choices[user_prob < pop[:, 0]] = -1
        votes_data.append(choices)
    return votes_data


def generate_populations_probabilities(n_comments=10,  n_populations=3):
    populations = []
    alphas = [0.2, 0.4, 0.6, 0.8, 1]
    for p in range(n_populations):
        alpha_index = (p % len(alphas))
        alpha = alphas[alpha_index]
        probabilities = np.random.dirichlet([alpha]*3, n_comments)
        populations.append(np.add.accumulate(probabilities,1))

    return populations


def get_n_comments(conversation):
    current_path = dirname(__file__)
    csv_file = join(current_path, 'polis_data/%s/summary.csv' % conversation)
    content = ''
    with open(csv_file, 'r') as f:
        content = f.read()

    REGEX = '.*comments,(\d+)*'
    match = re.search(REGEX, content)

    return int(match.groups()[0])


def get_n_participants(conversation):

    p_index = 0
    file_name = 'polis_data/%s/comments.csv' % conversation
    csv_file = join(dirname(__file__), file_name)

    with open(csv_file, 'r') as f:
        buffer = csv.reader(f, delimiter=',', quotechar='|')

        for index,line in enumerate(buffer, start=1):
            if(index > 1):
                p_index = max(p_index, int(line[2]))

    file_name = 'polis_data/%s/participants-votes.csv' % conversation
    csv_file = join(dirname(__file__), file_name)
    with open(csv_file, 'r') as f:
        buffer = csv.reader(f, delimiter=',', quotechar='|')

        for index,line in enumerate(buffer, start=1):
            if(index > 1):
                p_index = max(p_index, int(line[0]))

    return int(p_index) + 1


def get_votes(conversation):
    file_name = 'polis_data/%s/votes.csv' % conversation
    csv_file = join(dirname(__file__), file_name)

    n_comments = get_n_comments(conversation)
    n_participants = get_n_participants(conversation)
    rank = [n_participants, n_comments]
    dataset = np.zeros(rank)

    with open(csv_file, 'r') as f:
        buffer = csv.reader(f, delimiter=',', quotechar='|')

        for index,line in enumerate(buffer, start=1):
            if(index > 1):
                c_index = int(line[1])
                p_index = int(line[2])
                value = int(line[3])
                dataset[p_index][c_index] = value

    return dataset

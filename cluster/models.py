from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from django_extensions.db.models import TimeStampedModel

from sklearn.metrics import silhouette_score
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans

import numpy as np
import pandas as pd

from picklefield.fields import PickledObjectField


class Conversation(TimeStampedModel):
    id_conversation = models.BigIntegerField(primary_key=True)

    def n_participants(self):
        return Vote.participants_quantity(self)

    def n_comments(self):
        return self.comments.count()


class Comment(TimeStampedModel):
    id_comment = models.BigIntegerField(primary_key=True)
    conversation = models.ForeignKey(
        Conversation,
        related_name='comments',
        on_delete=models.CASCADE
    )
    content = models.CharField(max_length=140)


class Vote(TimeStampedModel):
    comment = models.ForeignKey(
        Comment,
        related_name='votes',
        on_delete=models.CASCADE
    )
    user_id = models.BigIntegerField(blank=False)
    value = models.SmallIntegerField(
        blank=False,
        validators=[MinValueValidator(-1), MaxValueValidator(1)]
    )

    class Meta:
        unique_together = ('comment', 'user_id')

    @classmethod
    def for_conversation(cls, conversation):
        return cls.objects.filter(comment__conversation_id=conversation.pk)

    @classmethod
    def participants_quantity(cls, conversation):
        votes = cls.for_conversation(conversation)
        seen_votes = {}
        for vote in votes:
            if vote.user_id not in seen_votes:
                seen_votes[vote.user_id] = vote.user_id

        return len(seen_votes)


class ClusterData(models.Model):
    conversation = models.ForeignKey(Conversation, related_name='clusters')
    n_clusters = models.IntegerField(null=True, blank=True)
    centroids = PickledObjectField(null=True, blank=True)
    silhouette = models.FloatField(null=True, blank=True)
    labels = PickledObjectField(null=True, blank=True)
    members = PickledObjectField(null=True, blank=True)

    def __str__(self):
        return """
            Conversation id: %s,\n
            N_clusters: %s\n,
            Labels: %s,
            """ % (self.conversation_id, self.n_clusters, self.labels)


class ClusterManager():
    def __init__(self, conversation, *args, **kwargs):
        self.conversation = conversation
        self.k_max = min(5, self.conversation.n_participants())
        self.n_components = 2

    def ready_to_math(self):
        return self.conversation.n_participants() > 3

    def get_reduced_dataset(self):
        data_matrix = ConversationDataMatrix(self.conversation)

        if(self.conversation.n_comments() > self.n_components):
            data_matrix.apply_pca(self.n_components)

        return data_matrix.dataset

    def compute_clusters(self):

        best_silhouette = -1
        best_kmeans = None
        dataset = self.get_reduced_dataset()

        for k in range(0, self.k_max):
            # number of clusters (k) goes from 1 to 5
            kmeans = compute_kmeans(dataset, k + 1)
            if(kmeans.silhouette > best_silhouette):
                best_silhouette = kmeans.silhouette
                best_kmeans = kmeans

        return self.save_clusters(best_silhouette, best_kmeans, dataset)

    def save_clusters(self, silhouette, kmeans, members):
        cluster_data, created = ClusterData.objects.get_or_create(
            conversation_id=self.conversation.pk,
            silhouette=silhouette,
            centroids=kmeans.cluster_centers_,
            n_clusters=kmeans.n_clusters,
            labels=kmeans.labels_,
            members=members
        )
        return self.build_clusters_array(cluster_data)

    def build_clusters_array(self, cluster_data):
        clusters = {}
        members = cluster_data.members
        indexes = members.index
        for member_index, member in enumerate(members.as_matrix()):
            user_info = {str(indexes[member_index]): [member[0], member[1]]}
            cluster_number = str(cluster_data.labels[member_index])
            if(cluster_number in clusters):
                clusters[cluster_number].append(user_info)
            else:
                clusters[cluster_number] = [user_info]

        return clusters


class ConversationDataMatrix():
    def __init__(self, conversation):
        self.conversation = conversation
        self.rank = [conversation.n_participants(), conversation.n_comments()]
        self.dataset = self.build_dataset()

    def apply_pca(self, n_components):
        decomposition = PCA(n_components=n_components)
        try:
            reduced_dataset = decomposition.fit_transform(self.dataset)

            dataset = pd.DataFrame(
                data=reduced_dataset,
                index=self.user_ids,
                columns=['X', 'Y'],
            )
            self.dataset = dataset
        except:
            return self.dataset

    def load_votes(self):
        if not(self.conversation):
            return None
        return Vote.objects.filter(comment__conversation_id=self.conversation.pk) \
                           .values('user_id', 'comment_id', 'value') \
                           .order_by()

    def build_dataset(self):
        if not(self.conversation):
            print('Failed building dataset for empty conversation')
            return None

        votes = self.load_votes()
        values = {}
        comments = {}
        for vote in votes:
            if vote['user_id'] not in values:
                values[vote['user_id']] = {}
            values[vote['user_id']][vote['comment_id']] = vote['value']
            comments[vote['comment_id']] = True

        indexes = list(values.keys())
        columns = list(comments.keys())
        dataset = pd.DataFrame(
            data=np.zeros([len(indexes), len(columns)]),
            # data=np.zeros(self.rank), # Guess that does not need self.rank
            index=indexes,
            columns=columns,
        )
        self.base_dataset = dataset
        self.user_ids = indexes

        for user_id, user_comments in values.items():
            for comment_id, value in user_comments.items():
                dataset.loc[user_id][comment_id] = value

        return dataset


def compute_kmeans(dataset, k):
    print("\n\nPrepare to calculate clusters for k = %s" % k)
    kmeans = KMeans(n_clusters=k, max_iter=300, verbose=0, random_state=0)
    kmeans.silhouette = 0
    kmeans.cluster_centers_ = []
    kmeans.labels_ = []

    try:
        kmeans.fit_predict(dataset)
        print("Successfull kmeans calculation for k = %s" % k)
        kmeans.silhouette = compute_silhouette(dataset, kmeans.labels_, k)
    except:
        print("Could not calculate clusters for k = %s" % k)

    return kmeans


def compute_silhouette(dataset, labels, k):
    silhouette = 0
    try:
        silhouette = silhouette_score(dataset, labels)
        print("Successfull silhouette calculation for k = %s" % k)
    except:
        print("Could not calculate silhouette for k = %s" % k)

    return silhouette

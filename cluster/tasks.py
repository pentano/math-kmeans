from __future__ import absolute_import, unicode_literals
from django.db import IntegrityError

from celery import shared_task

from .models import Conversation, Comment, Vote, ClusterManager


@shared_task(name='pentano.new_conversation')
def new_conversation(id_conversation):
    try:
        Conversation.objects.create(id_conversation=id_conversation)
        return (True, 'Conversation created successfully.')
    except Exception as error:
        return (False, str(error))


@shared_task(name='pentano.add_comment')
def add_comment(id_conversation, id_comment, text):
    try:
        conversation = Conversation.objects.get(
            id_conversation=id_conversation
        )
        Comment.objects.create(
            id_comment=id_comment,
            conversation=conversation,
            content=text
        )
        return (True, 'Comment created successfully.')
    except Exception as error:
        return (False, str(error))


@shared_task(name='pentano.add_vote')
def add_vote(comment_id, user_id, value):
    try:
        Vote.objects.create(
            comment_id=comment_id,
            user_id=user_id,
            value=value,
        )
    except IntegrityError:
        vote = Vote.objects.get(
            comment_id=comment_id,
            user_id=user_id,
        )
        vote.value = value
        vote.save()
        return (True, 'Votes updated successfully.')
    else:
        return (True, 'Votes created successfully.')


@shared_task(name='pentano.get_cluster')
def get_cluster(id_conversation, user_ids=[]):
    try:
        conversation = Conversation.objects.get(pk=id_conversation)
        cluster_data = ClusterManager(conversation).compute_clusters()
        return (True, cluster_data)
    except Exception as error:
        return (False, str(error))


@shared_task(name='pentano.delete_conversation')
def delete_conversation(id_conversation):
    try:
        conversation = Conversation.objects.get(pk=id_conversation)
        conversation.delete()
        return (True)
    except Exception as error:
        return (False, str(error))


@shared_task(name='pentano.delete_comment')
def delete_comment(id_comment):
    try:
        comment = Comment.objects.get(pk=id_comment)
        comment.delete()
        return (True)
    except Exception as error:
        return (False, str(error))
